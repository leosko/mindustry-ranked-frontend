"use strict";

const isDevelopment = process.env.NODE_ENV == "development";

exports.isDevelopment = isDevelopment;
