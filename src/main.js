require("log-timestamp");
Vue.config.devtools = process.env.NODE_ENV === 'development'
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueTimers from 'vue-timers'
import App from './App.vue'
import Leaderboard from './components/leaderboard/Leaderboard'
import Matches from './components/Matches'
import FullMatch from './components/FullMatch'
import Player from './components/Player'
import Profile from './components/Profile'
import Search from './components/search/Search'
import Login from './components/Login'

Vue.use(VueRouter)
Vue.use(VueTimers)

const routes = [
  { name: 'home', path: '/', component: Leaderboard, props: true },
  { name: 'leaderboard', path: '/leaderboard/:page', component: Leaderboard, props: true },
  { name: 'matches', path: '/matches/:page', component: Matches, props: true },
  { name: 'player', path: '/player/:player_id/:page', component: Player, props: true },
  { name: 'match', path: '/match/:match_id', component: FullMatch, props: true },
  { name: 'profile', path: '/profile', component: Profile, props: true },
  { name: 'search', path: '/search', component: Search, props: true },
  { name: 'login', path: '/login', component: Login },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
