const axios = require("axios");
var rankutils = require("../helpers/rankutils");
const AdmZip = require("adm-zip");
const { decodeBuildOrder } = require("../helpers/buildorder");

const domainUrl = "https://ranked.ddns.net/";
const url = `${domainUrl}api/`;
const viewLimit = 10;

const getPlayerDelta = (match, player_id) => {
    if (typeof match.finished_at === "undefined" || match.finished_at == null) {
        return null;
    }
    if (player_id == match.team1_player_id) {
        return Math.sign(match.elo1_after - match.elo1_before);
    } else {
        return Math.sign(match.elo2_after - match.elo2_before);
    }
};
const getPlayerEloBefore = (match, playerId) => {
    if (playerId == match.team1_player_id) {
        return match.elo1_before;
    } else {
        return match.elo2_before;
    }
};
const getPlayerEloAfter = (match, playerId) => {
    if (playerId == match.team1_player_id) {
        return match.elo1_after;
    } else {
        return match.elo2_after;
    }
};

class Api {
    static getLeaderboardPlayers(page) {
        return new Promise(async (resolve, reject) => {
            try {
                var res = await axios.get(`${url}leaderboard?limit=${viewLimit}&skip=${(page - 1) * viewLimit}`).then((res) => res.data);
                if (res.error) {
                    reject(res.error);
                } else {
                    res.playerList[0].place = res.startingPlace;
                    res.playerList[0].rank = rankutils.get_rank(res.playerList[0].rating);
                    for (var i = 1; i < res.playerList.length; i++) {
                        res.playerList[i].place =
                            res.playerList[i].rating == res.playerList[i - 1].rating ? res.playerList[i - 1].place : res.startingPlace + i;
                        res.playerList[i].rank = rankutils.get_rank(res.playerList[i].rating);
                    }
                    resolve(res);
                }
            } catch (err) {
                reject(err);
            }
        });
    }
    static getPlayer(id) {
        return new Promise(async (resolve, reject) => {
            try {
                var res = await axios.get(`${url}player?playerId=${id}`).then((res) => res.data);
                if (res.error) {
                    reject(res.error);
                } else {
                    res.rank = rankutils.get_rank(res.rating);
                    resolve(res);
                }
            } catch (err) {
                reject(err);
            }
        });
    }
    static getPlayerMatches(player_id, page) {
        return new Promise(async (resolve, reject) => {
            try {
                var res = await axios.get(`${url}playerMatches?matchType=1v1&playerId=${player_id}&limit=${viewLimit}&skip=${(page - 1) * viewLimit}`).then((res) => res.data);
                if (res.error) {
                    reject(res.error);
                } else {
                    for (var i = 0; i < res.history.length; i++) {
                        res.history[i].is_winner =
                            res.history[i].team1_player_id == player_id
                                ? res.history[i].result == res.history[i].team1_id
                                : res.history[i].team2_player_id == player_id
                                ? res.history[i].result == res.history[i].team2_id
                                : false;
                        res.history[i].is_tie = res.history[i].result == -7; // magic
                        res.history[i].is_error = res.history[i].result == -9; // magic x2

                        res.history[i].playerTeam = res.team1_player_id == player_id ? 1 : 2;
                        res.history[i].playerDelta = getPlayerDelta(res.history[i], player_id);
                        res.history[i].playerEloBefore = getPlayerEloBefore(res.history[i], player_id);
                        res.history[i].playerEloAfter = getPlayerEloAfter(res.history[i], player_id);
                        
                        res.history[i].winner_team_number = (res.history[i].result == res.history[i].team1_id) ? 1 : (res.history[i].result == res.history[i].team2_id) ? 2 : 0;
                        res.history[i].in_progress = (!res.history[i].finished_at);
                        res.history[i].rank1 = rankutils.get_rank(res.history[i].elo1_before);
                        res.history[i].rank2 = rankutils.get_rank(res.history[i].elo2_before);
                    }
                    resolve(res);
                }
            } catch (err) {
                reject(err);
            }
        });
    }
    static searchPlayers(query, page) {
        return new Promise(async (resolve, reject) => {
            try {
                var res = await axios.get(`${url}search?name=${query}&limit=${viewLimit}&skip=${(page - 1) * viewLimit}`).then((res) => res.data);
                if (res.error) {
                    reject(res.error);
                } else {
                    resolve(res);
                }
            } catch (err) {
                reject(err);
            }
        });
    }
    static getMatches(page) {
        return new Promise(async (resolve, reject) => {
            try {
                var res = await axios.get(`${url}matches?matchType=1v1&limit=${viewLimit}&skip=${(page - 1) * viewLimit}`).then((res) => res.data);
                if (res.error) {
                    reject(res.error);
                } else {
                    for (var i = 0; i < res.matches.length; i++) {
                        res.matches[i].winner_team_number = (res.matches[i].result == res.matches[i].team1_id) ? 1 : (res.matches[i].result == res.matches[i].team2_id) ? 2 : 0;
                        res.matches[i].in_progress = (!res.matches[i].finished_at);
                        res.matches[i].rank1 = rankutils.get_rank(res.matches[i].elo1_before);
                        res.matches[i].rank2 = rankutils.get_rank(res.matches[i].elo2_before);
                    }
                    resolve(res);
                }
            } catch (err) {
                reject(err);
            }
        });
    }
    static getMatch(matchId) {
        return new Promise(async (resolve, reject) => {
            try {
                var res = await axios.get(`${url}match?matchId=${matchId}`).then((res) => res.data);
                if (res.error) {
                    reject(res);
                } else {
                    res.winner_team_number = (res.result == res.team1_id) ? 1 : (res.result == res.team2_id) ? 2 : 0;
                    res.in_progress = (!res.finished_at);
                    res.rank1 = rankutils.get_rank(res.elo1_before);
                    res.rank2 = rankutils.get_rank(res.elo2_before);
                    res.mapName = res.map.replace(/_/g, " ");
                    resolve(res);
                }
            } catch (err) {
                reject(err);
            }
        });
    }
    static getMatchStats(matchId) {
        return new Promise(async (resolve, reject) => {
            try {
                var res = await axios.get(`${url}matchStats?matchId=${matchId}`).then((res) => res.data);
                if (res.error) {
                    reject(res);
                } else {
                    resolve(res);
                }
            } catch (err) {
                reject(err);
            }
        });
    }
    static getDecodedReplay(match) {
        return new Promise(async (resolve, reject) => {
            try {
                var res = await axios.get(`${domainUrl}replay/${match.match_id}.zip`, {
                    responseType: 'arraybuffer'
                })
                .then(res => Buffer.from(res.data, 'binary'));
                var archive = new AdmZip(res);
                var entries = archive.getEntries();
                var bo = decodeBuildOrder(entries[0].getData());
                resolve(bo);
            } catch (err) {
                reject(err);
            }
        });
    }
    static getReplaySize(matchId) {
        return new Promise(async (resolve, reject) => {
            try {
                var res = await axios.head(`${domainUrl}replay/${matchId}.zip`);
                resolve(res.headers["content-length"]);
            } catch (err) {
                reject(err);
            }
        });
    }
    static getReplayGifSize(matchId) {
        return new Promise(async (resolve, reject) => {
            try {
                var res = await axios.head(`${domainUrl}replay-gif/${matchId}.gif`);
                resolve(res.headers["content-length"]);
            } catch (err) {
                reject(err);
            }
        });
    }
}

export default Api;
