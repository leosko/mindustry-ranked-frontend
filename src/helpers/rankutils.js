"use strict";

const ranks = [];
ranks.push({ startsAt: Number.MIN_SAFE_INTEGER, name: "Bronze\xa0#3", color: "#a57655" });
ranks.push({ startsAt: 1200, name: "Bronze\xa0#2", color: "#a57655" });
ranks.push({ startsAt: 1400, name: "Bronze\xa0#1", color: "#a57655" });
ranks.push({ startsAt: 1600, name: "Silver\xa0#3", color: "#c0c0c0" });
ranks.push({ startsAt: 1800, name: "Silver\xa0#2", color: "#c0c0c0" });
ranks.push({ startsAt: 1900, name: "Silver\xa0#1", color: "#c0c0c0" });
ranks.push({ startsAt: 2000, name: "Gold\xa0#3", color: "#ddc34d" });
ranks.push({ startsAt: 2100, name: "Gold\xa0#2", color: "#ddc34d" });
ranks.push({ startsAt: 2200, name: "Gold\xa0#1", color: "#ddc34d" });
ranks.push({ startsAt: 2300, name: "Platinum\xa0#3", color: "#47c8e6" });
ranks.push({ startsAt: 2400, name: "Platinum\xa0#2", color: "#47c8e6" });
ranks.push({ startsAt: 2500, name: "Platinum\xa0#1", color: "#47c8e6" });
ranks.push({ startsAt: 2700, name: "Diamond\xa0#3", color: "#a16fec" });
ranks.push({ startsAt: 2900, name: "Diamond\xa0#2", color: "#a16fec" });
ranks.push({ startsAt: 3100, name: "Diamond\xa0#1", color: "#a16fec" });
ranks.push({ startsAt: 3500, name: "Master", color: "ff4246" });
ranks.push({ startsAt: 70000, name: "Anuke", color: "black" });

for (var i = 0; i < ranks.length; i++) {
  ranks[i].shortName = ranks[i].name.replace('\xa0', ' ');
}
for (i = 0; i < ranks.length - 1; i++) {
  ranks[i].endsAt = ranks[i + 1].startsAt - 1;
}
ranks[ranks.length - 1].endsAt = Number.MAX_SAFE_INTEGER;

exports.get_rank = (rating) => {
  if (rating < ranks[1].startsAt) {
    return ranks[0];
  }
  for (var i = 2; i < ranks.length; i++) {
    if (ranks[i].startsAt > rating) {
      return ranks[i - 1];
    }
  }
  return ranks[ranks.length - 1];
};

exports.ranks = ranks;