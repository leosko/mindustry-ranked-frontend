String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - hours * 3600) / 60);
    var seconds = sec_num - hours * 3600 - minutes * 60;
  
    if (hours < 10) {
      hours = "0" + hours;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return hours + ":" + minutes + ":" + seconds;
  };
  
  const saveInterval = parseInt($('#replay').attr('save-interval'));
  const getTime = (frameNumber) => {
    return (frameNumber * saveInterval / 1000).toString().toHHMMSS();
  }
  
  const activateReplay = (url) => {
    $("#img-replay").toggleClass("d-none");
    var totalLength;
    var rub = new SuperGif({
      gif: $("#img-replay")[0],
      max_width: 500,
      draw_while_loading: true,
      progressbar_height: 1,
      frame_change_callback: (currentFrame, totalFrames) => {
        $("#progress")[0].value = currentFrame;
        $("#progress-time").html(
          `${getTime(currentFrame)} / ${totalLength}`
        );
      },
    });
    rub.load_url(url, () => {
      $('#show-replay').toggleClass('d-none');
      $("#progress-controls").toggleClass('d-none');
      $("#progress-controls").toggleClass('d-inline-block');
      var length = rub.get_length();
      $("#progress")[0].max = length - 1;
      totalLength = getTime(length - 1);
      $("#progress-time").html(`?? / ${totalLength}`);
    });
  
    $("#play").on("click", () => {
      if (rub.get_loading()) {
        return;
      }
      rub.play();
    });
    $("#pause").on("click", () => {
      if (rub.get_loading()) {
        return;
      }
      rub.pause();
      $("#play").toggleClass("d-inline-block");
      $("#pause").toggleClass("d-none");
    });
    $("#step-forward").on("click", () => {
      if (rub.get_loading() || rub.get_current_frame() == rub.get_length() - 1) {
        return;
      }
      if (rub.get_playing()) {
        rub.pause();
      }
      rub.move_relative(1);
    });
    $("#step-backward").on("click", () => {
      if (rub.get_loading() || rub.get_current_frame() == 0) {
        return;
      }
      if (rub.get_playing()) {
        rub.pause();
      }
      rub.move_relative(-1);
    });
  
    $("#progress").on("input", (e) => {
      if (rub.get_loading()) {
        return;
      }
      if (rub.get_playing()) {
        $("#pause").click();
      }
      rub.move_to(e.target.value);
    });
  
    $(document).bind("keydown", (e) => {
      if (rub.get_loading()) {
        return;
      }
      var key = e.keyCode ? e.keyCode : e.charCode;
      console.log(key);
      if (key == 32 || key == 75 || key == 83) {
        // space, s, k
        if (rub.get_playing()) {
          $("#pause").click();
        } else {
          $("#play").click();
        }
      } else if (key == 97 || key == 37 || key == 74) {
        // left, a, j
        $("#step-backward").click();
      } else if (key == 100 || key == 39 || key == 76) {
        // right, d, l
        $("#step-forward").click();
      }
    });
    return true;
  }
  
  const addImageDownload = (url, size) => {
    $('#replay-size').html(humanFileSize(size));
    $('#replay').toggleClass('d-none');
    $('#download-replay').attr('href', url);
  
    $('#show-replay').on('click', () => {
      $('#show-replay').attr('disabled', true);
      $('#img-loader').toggleClass('d-none');
      if (!activateReplay(url)) {
        $('#img-replay').on('load', () => {
          $('#show-replay').toggleClass('d-none');
          $('#img-loader').toggleClass('d-none');
        });
        $('#img-replay').attr('src', url);
      }
    });
  }
  