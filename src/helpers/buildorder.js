const root = require("./ReplayMessages").root;
const protobuf = require("protobufjs");

module.exports.blockMap = {"air":0,"spawn":1,"cliff":2,"build1":3,"build2":4,"build3":5,"build4":6,"build5":7,"build6":8,"build7":9,"build8":10,"build9":11,"build10":12,"build11":13,"build12":14,"build13":15,"build14":16,"build15":17,"build16":18,"deepwater":19,"water":20,"tainted-water":21,"darksand-tainted-water":22,"sand-water":23,"darksand-water":24,"tar":25,"slag":26,"space":27,"stone":28,"craters":29,"char":30,"basalt":31,"hotrock":32,"magmarock":33,"sand":34,"darksand":35,"dirt":36,"mud":37,"dacite":38,"grass":39,"salt":40,"snow":41,"ice":42,"ice-snow":43,"shale":44,"stone-wall":45,"spore-wall":46,"dirt-wall":47,"dacite-wall":48,"ice-wall":49,"snow-wall":50,"dune-wall":51,"sand-wall":52,"salt-wall":53,"shrubs":54,"shale-wall":55,"spore-pine":56,"snow-pine":57,"pine":58,"white-tree-dead":59,"white-tree":60,"spore-cluster":61,"boulder":62,"snow-boulder":63,"shale-boulder":64,"sand-boulder":65,"dacite-boulder":66,"basalt-boulder":67,"moss":68,"spore-moss":69,"metal-floor":70,"metal-floor-damaged":71,"metal-floor-2":72,"metal-floor-3":73,"metal-floor-5":74,"dark-panel-1":75,"dark-panel-2":76,"dark-panel-3":77,"dark-panel-4":78,"dark-panel-5":79,"dark-panel-6":80,"dark-metal":81,"pebbles":82,"tendrils":83,"ore-copper":84,"ore-lead":85,"ore-scrap":86,"ore-coal":87,"ore-titanium":88,"ore-thorium":89,"graphite-press":90,"multi-press":91,"silicon-smelter":92,"silicon-crucible":93,"kiln":94,"plastanium-compressor":95,"phase-weaver":96,"alloy-smelter":97,"cryofluid-mixer":98,"pyratite-mixer":99,"blast-mixer":100,"melter":101,"separator":102,"disassembler":103,"spore-press":104,"pulverizer":105,"coal-centrifuge":106,"incinerator":107,"copper-wall":108,"copper-wall-large":109,"titanium-wall":110,"titanium-wall-large":111,"plastanium-wall":112,"plastanium-wall-large":113,"thorium-wall":114,"thorium-wall-large":115,"phase-wall":116,"phase-wall-large":117,"surge-wall":118,"surge-wall-large":119,"door":120,"door-large":121,"scrap-wall":122,"scrap-wall-large":123,"scrap-wall-huge":124,"scrap-wall-gigantic":125,"thruster":126,"mender":127,"mend-projector":128,"overdrive-projector":129,"overdrive-dome":130,"force-projector":131,"shock-mine":132,"conveyor":133,"titanium-conveyor":134,"plastanium-conveyor":135,"armored-conveyor":136,"junction":137,"bridge-conveyor":138,"phase-conveyor":139,"sorter":140,"inverted-sorter":141,"router":142,"distributor":143,"overflow-gate":144,"underflow-gate":145,"mass-driver":146,"payload-conveyor":147,"payload-router":148,"mechanical-pump":149,"rotary-pump":150,"thermal-pump":151,"conduit":152,"pulse-conduit":153,"plated-conduit":154,"liquid-router":155,"liquid-tank":156,"liquid-junction":157,"bridge-conduit":158,"phase-conduit":159,"power-node":160,"power-node-large":161,"surge-tower":162,"diode":163,"battery":164,"battery-large":165,"combustion-generator":166,"thermal-generator":167,"steam-generator":168,"differential-generator":169,"rtg-generator":170,"solar-panel":171,"solar-panel-large":172,"thorium-reactor":173,"impact-reactor":174,"mechanical-drill":175,"pneumatic-drill":176,"laser-drill":177,"blast-drill":178,"water-extractor":179,"cultivator":180,"oil-extractor":181,"core-shard":182,"core-foundation":183,"core-nucleus":184,"vault":185,"container":186,"unloader":187,"duo":188,"scatter":189,"scorch":190,"hail":191,"wave":192,"lancer":193,"arc":194,"parallax":195,"swarmer":196,"salvo":197,"segment":198,"tsunami":199,"fuse":200,"ripple":201,"cyclone":202,"foreshadow":203,"spectre":204,"meltdown":205,"command-center":206,"ground-factory":207,"air-factory":208,"naval-factory":209,"additive-reconstructor":210,"multiplicative-reconstructor":211,"exponential-reconstructor":212,"tetrative-reconstructor":213,"repair-point":214,"resupply-point":215,"power-source":216,"power-void":217,"item-source":218,"item-void":219,"liquid-source":220,"liquid-void":221,"illuminator":222,"legacy-mech-pad":223,"legacy-unit-factory":224,"legacy-unit-factory-air":225,"legacy-unit-factory-ground":226,"launch-pad":227,"launch-pad-large":228,"interplanetary-accelerator":229,"message":230,"switch":231,"micro-processor":232,"logic-processor":233,"hyper-processor":234,"memory-cell":235,"memory-bank":236,"logic-display":237,"large-logic-display":238,"block-forge":239,"block-loader":240,"block-unloader":241};
module.exports.unitMap = {"dagger":0,"mace":1,"fortress":2,"scepter":3,"reign":4,"nova":5,"pulsar":6,"quasar":7,"vela":8,"corvus":9,"crawler":10,"atrax":11,"spiroct":12,"arkyid":13,"toxopid":14,"flare":15,"horizon":16,"zenith":17,"antumbra":18,"eclipse":19,"mono":20,"poly":21,"mega":22,"quad":23,"oct":24,"risso":25,"minke":26,"bryde":27,"sei":28,"omura":29,"alpha":30,"beta":31,"gamma":32,"block":33};
module.exports.blockNameMap = {0:"air",1:"spawn",2:"cliff",3:"build1",4:"build2",5:"build3",6:"build4",7:"build5",8:"build6",9:"build7",10:"build8",11:"build9",12:"build10",13:"build11",14:"build12",15:"build13",16:"build14",17:"build15",18:"build16",19:"deepwater",20:"water",21:"tainted-water",22:"darksand-tainted-water",23:"sand-water",24:"darksand-water",25:"tar",26:"slag",27:"space",28:"stone",29:"craters",30:"char",31:"basalt",32:"hotrock",33:"magmarock",34:"sand",35:"darksand",36:"dirt",37:"mud",38:"dacite",39:"grass",40:"salt",41:"snow",42:"ice",43:"ice-snow",44:"shale",45:"stone-wall",46:"spore-wall",47:"dirt-wall",48:"dacite-wall",49:"ice-wall",50:"snow-wall",51:"dune-wall",52:"sand-wall",53:"salt-wall",54:"shrubs",55:"shale-wall",56:"spore-pine",57:"snow-pine",58:"pine",59:"white-tree-dead",60:"white-tree",61:"spore-cluster",62:"boulder",63:"snow-boulder",64:"shale-boulder",65:"sand-boulder",66:"dacite-boulder",67:"basalt-boulder",68:"moss",69:"spore-moss",70:"metal-floor",71:"metal-floor-damaged",72:"metal-floor-2",73:"metal-floor-3",74:"metal-floor-5",75:"dark-panel-1",76:"dark-panel-2",77:"dark-panel-3",78:"dark-panel-4",79:"dark-panel-5",80:"dark-panel-6",81:"dark-metal",82:"pebbles",83:"tendrils",84:"ore-copper",85:"ore-lead",86:"ore-scrap",87:"ore-coal",88:"ore-titanium",89:"ore-thorium",90:"graphite-press",91:"multi-press",92:"silicon-smelter",93:"silicon-crucible",94:"kiln",95:"plastanium-compressor",96:"phase-weaver",97:"alloy-smelter",98:"cryofluid-mixer",99:"pyratite-mixer",100:"blast-mixer",101:"melter",102:"separator",103:"disassembler",104:"spore-press",105:"pulverizer",106:"coal-centrifuge",107:"incinerator",108:"copper-wall",109:"copper-wall-large",110:"titanium-wall",111:"titanium-wall-large",112:"plastanium-wall",113:"plastanium-wall-large",114:"thorium-wall",115:"thorium-wall-large",116:"phase-wall",117:"phase-wall-large",118:"surge-wall",119:"surge-wall-large",120:"door",121:"door-large",122:"scrap-wall",123:"scrap-wall-large",124:"scrap-wall-huge",125:"scrap-wall-gigantic",126:"thruster",127:"mender",128:"mend-projector",129:"overdrive-projector",130:"overdrive-dome",131:"force-projector",132:"shock-mine",133:"conveyor",134:"titanium-conveyor",135:"plastanium-conveyor",136:"armored-conveyor",137:"junction",138:"bridge-conveyor",139:"phase-conveyor",140:"sorter",141:"inverted-sorter",142:"router",143:"distributor",144:"overflow-gate",145:"underflow-gate",146:"mass-driver",147:"payload-conveyor",148:"payload-router",149:"mechanical-pump",150:"rotary-pump",151:"thermal-pump",152:"conduit",153:"pulse-conduit",154:"plated-conduit",155:"liquid-router",156:"liquid-tank",157:"liquid-junction",158:"bridge-conduit",159:"phase-conduit",160:"power-node",161:"power-node-large",162:"surge-tower",163:"diode",164:"battery",165:"battery-large",166:"combustion-generator",167:"thermal-generator",168:"steam-generator",169:"differential-generator",170:"rtg-generator",171:"solar-panel",172:"solar-panel-large",173:"thorium-reactor",174:"impact-reactor",175:"mechanical-drill",176:"pneumatic-drill",177:"laser-drill",178:"blast-drill",179:"water-extractor",180:"cultivator",181:"oil-extractor",182:"core-shard",183:"core-foundation",184:"core-nucleus",185:"vault",186:"container",187:"unloader",188:"duo",189:"scatter",190:"scorch",191:"hail",192:"wave",193:"lancer",194:"arc",195:"parallax",196:"swarmer",197:"salvo",198:"segment",199:"tsunami",200:"fuse",201:"ripple",202:"cyclone",203:"foreshadow",204:"spectre",205:"meltdown",206:"command-center",207:"ground-factory",208:"air-factory",209:"naval-factory",210:"additive-reconstructor",211:"multiplicative-reconstructor",212:"exponential-reconstructor",213:"tetrative-reconstructor",214:"repair-point",215:"resupply-point",216:"power-source",217:"power-void",218:"item-source",219:"item-void",220:"liquid-source",221:"liquid-void",222:"illuminator",223:"legacy-mech-pad",224:"legacy-unit-factory",225:"legacy-unit-factory-air",226:"legacy-unit-factory-ground",227:"launch-pad",228:"launch-pad-large",229:"interplanetary-accelerator",230:"message",231:"switch",232:"micro-processor",233:"logic-processor",234:"hyper-processor",235:"memory-cell",236:"memory-bank",237:"logic-display",238:"large-logic-display",239:"block-forge",240:"block-loader",241:"block-unloader"};
module.exports.unitNameMap = {0:"dagger",1:"mace",2:"fortress",3:"scepter",4:"reign",5:"nova",6:"pulsar",7:"quasar",8:"vela",9:"corvus",10:"crawler",11:"atrax",12:"spiroct",13:"arkyid",14:"toxopid",15:"flare",16:"horizon",17:"zenith",18:"antumbra",19:"eclipse",20:"mono",21:"poly",22:"mega",23:"quad",24:"oct",25:"risso",26:"minke",27:"bryde",28:"sei",29:"omura",30:"alpha",31:"beta",32:"gamma",33:"block"};
module.exports.itemNameToId = {"copper":0,"lead":1,"metaglass":2,"graphite":3,"sand":4,"coal":5,"titanium":6,"thorium":7,"scrap":8,"silicon":9,"plastanium":10,"phase-fabric":11,"surge-alloy":12,"spore-pod":13,"blast-compound":14,"pyratite":15};
module.exports.itemIdToName = {0:"copper",1:"lead",2:"metaglass",3:"graphite",4:"sand",5:"coal",6:"titanium",7:"thorium",8:"scrap",9:"silicon",10:"plastanium",11:"phase-fabric",12:"surge-alloy",13:"spore-pod",14:"blast-compound",15:"pyratite"};
module.exports.itemIdToColor = {0:"d99d73ff",1:"8c7fa9ff",2:"ebeef5ff",3:"b2c6d2ff",4:"f7cba4ff",5:"272727ff",6:"8da1e3ff",7:"f9a3c7ff",8:"777777ff",9:"53565cff",10:"cbd97fff",11:"f4ba6eff",12:"f3e979ff",13:"7457ceff",14:"ff795eff",15:"ffaa5fff"};
module.exports.playerUnitsIds = [this.unitMap.alpha, this.unitMap.beta, this.unitMap.gamma];
module.exports.drills = [this.blockMap["blast-drill"], this.blockMap["pneumatic-drill"], this.blockMap["mechanical-drill"], this.blockMap["laser-drill"]];

module.protoMessages = {};
module.protoMessages.ProtoReplayItemType = root.lookupEnum("ProtoReplayItemType");
module.protoMessages.ProtoReplay = root.lookupType("ProtoReplay");
module.protoMessages.ProtoBlock = root.lookupType("ProtoBlock");
module.protoMessages.ProtoResourceItem = root.lookupType("ProtoResourceItem");
module.protoMessages.ProtoCoreResources = root.lookupType("ProtoCoreResources");
module.protoMessages.ProtoUnitChange = root.lookupType("ProtoUnitChange");
module.protoMessages.ProtoPlayerConnection = root.lookupType("ProtoPlayerConnection");
module.protoMessages.ProtoInfo = root.lookupType("ProtoInfo");
module.protoMessages.ProtoResourceTransfer = root.lookupType("ProtoResourceTransfer");
module.protoMessages.ProtoUnitPosition = root.lookupType("ProtoUnitPosition");
module.protoMessages.ProtoUnit = root.lookupType("ProtoUnit");
module.protoMessages.ProtoTieBreaker = root.lookupType("ProtoTieBreaker");
module.protoMessages.ProtoChat = root.lookupType("ProtoChat");
module.protoMessages.ProtoSchematicScheduled = root.lookupType("ProtoSchematicScheduled");
module.protoMessages.ProtoGameOver = root.lookupType("ProtoGameOver");

module.exports.decodeBuildOrder = function (buildOrder, types = null) {
    var res = [];
    var reader = protobuf.Reader.create(buildOrder);
    while (reader.pos < reader.len) {
        var protoReplay = module.protoMessages.ProtoReplay.decodeDelimited(reader);
        if (types != null && !types.includes(protoReplay.type)) {
            continue;
        }
        var item = { time: protoReplay.time, type: protoReplay.type, teamId: protoReplay.teamId};
        switch (protoReplay.type) {
            case module.protoMessages.ProtoReplayItemType.values.BLOCK_BUILD_BEGIN:
            case module.protoMessages.ProtoReplayItemType.values.BLOCK_BUILD_END:
            case module.protoMessages.ProtoReplayItemType.values.BLOCK_DESTROY:
                Object.assign(item, module.protoMessages.ProtoBlock.decode(protoReplay.item));
                break;
            case module.protoMessages.ProtoReplayItemType.values.UNIT_CREATE:
            case module.protoMessages.ProtoReplayItemType.values.UNIT_DESTROY:
            case module.protoMessages.ProtoReplayItemType.values.UNIT_UNLOAD:
                Object.assign(item, module.protoMessages.ProtoUnit.decode(protoReplay.item));
                item.unitName = module.exports.unitNameMap[item.id];
                break;
            case module.protoMessages.ProtoReplayItemType.values.PLAYER_JOIN:
            case module.protoMessages.ProtoReplayItemType.values.PLAYER_LEAVE:
                Object.assign(item, module.protoMessages.ProtoPlayerConnection.decode(protoReplay.item));
                break;
            case module.protoMessages.ProtoReplayItemType.values.DEPOSIT:
            case module.protoMessages.ProtoReplayItemType.values.WITHDRAW:
                Object.assign(item, module.protoMessages.ProtoResourceTransfer.decode(protoReplay.item));
                break;
            case module.protoMessages.ProtoReplayItemType.values.INFO:
                Object.assign(item, module.protoMessages.ProtoInfo.decode(protoReplay.item));
                break;
            case module.protoMessages.ProtoReplayItemType.values.CORE_RESOURCES:
                Object.assign(item, module.protoMessages.ProtoCoreResources.decode(protoReplay.item));
                break;
            case module.protoMessages.ProtoReplayItemType.values.TIE_BREAKER:
                Object.assign(item, module.protoMessages.ProtoTieBreaker.decode(protoReplay.item));
                break;
            case module.protoMessages.ProtoReplayItemType.values.CHAT:
                Object.assign(item, module.protoMessages.ProtoChat.decode(protoReplay.item));
                break;
            case module.protoMessages.ProtoReplayItemType.values.GAMEOVER:
                Object.assign(item, module.protoMessages.ProtoGameOver.decode(protoReplay.item));
                break;
            case module.protoMessages.ProtoReplayItemType.values.MECH_CHANGE:
            case module.protoMessages.ProtoReplayItemType.values.UNIT_POSITION:
            case module.protoMessages.ProtoReplayItemType.values.PLAYER_STATE:
            case module.protoMessages.ProtoReplayItemType.values.SCHEMATIC_PLANNED:
                break;
            default:
                console.log("UNKNOWN type: " + protoReplay.type);
                break;
        }
        if (item) {
            res.push(item);
        }
    }
    return res;
}

module.exports.aggregateBuildOrder = function (buildOrder) {
    var res = [ ];
    var currentItems = [];
    var lastTime = 0;
    var replayItemId = 0;
    for (var i in buildOrder) {
        var boItem = buildOrder[i];
        var currentItem = null;
        if (lastTime < boItem.time + 30000) {
            for (var j = 0; j < currentItems.length; j++) {
                if (currentItems[j].type == boItem.type
                    && currentItems[j].breaking == boItem.breaking
                    && currentItems[j].teamId == boItem.teamId
                    && currentItems[j].blockId == boItem.blockId
                    && currentItems[j].unitId == boItem.unitId) {
                    currentItem = currentItems[j];
                    lastTime = boItem.time;
                    break;
                }
            }
        }
        if (currentItem != null) {
            currentItem.count++;
            continue;
        } else {
            if (i > 0 && currentItems.length >= 6) {
                res.push(currentItems[0]);
                currentItems = currentItems.splice(1);
            }
            currentItems.push(boItem);
            boItem.replayItemId = replayItemId++;
            delete boItem.x;
            delete boItem.y;
            boItem.count = 1;
        }
    }
    for (var j = 0; j < currentItems.length; j++) {
        res.push(currentItems[j]);
    }
    return res;
}