/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/light");

var $root = ($protobuf.roots["default"] || ($protobuf.roots["default"] = new $protobuf.Root()))
.addJSON({
  com: {
    nested: {
      seapy: {
        nested: {
          replay: {
            options: {
              java_package: "com.seapy.replay",
              java_outer_classname: "ProtoReplayItems"
            },
            nested: {
              ProtoReplayItemType: {
                values: {
                  INFO: 0,
                  BLOCK_BUILD_BEGIN: 1,
                  BLOCK_BUILD_END: 2,
                  BLOCK_DESTROY: 3,
                  UNIT_CREATE: 4,
                  UNIT_DESTROY: 5,
                  PLAYER_JOIN: 6,
                  PLAYER_LEAVE: 7,
                  DEPOSIT: 8,
                  WITHDRAW: 9,
                  MECH_CHANGE: 10,
                  UNIT_POSITION: 11,
                  CORE_RESOURCES: 12,
                  CHAT: 13,
                  TIE_BREAKER: 14,
                  PLAYER_STATE: 15,
                  UNIT_UNLOAD: 16,
                  GAMEOVER: 17,
                  SCHEMATIC_PLANNED: 18
                }
              },
              ProtoGameOverReason: {
                values: {
                  VICTORY: 0,
                  ABANDONED: 1,
                  TIEBREAKER: 2,
                  SURRENDER: 3,
                  ERROR: 4
                }
              },
              ProtoInfoRecordName: {
                values: {
                  PLAYER_INFO_INTERVAL: 1,
                  UNIT_INFO_INTERVAL: 2,
                  CORE_RESOURCES_INFO_INTERVAL: 3,
                  STARTED_AT: 4,
                  MAP_NAME: 5,
                  PLAYER_NAME: 6,
                  PLAYER_TEAM: 7,
                  PLAYER_RATING: 8,
                  TYPE: 9,
                  LICENSE: 10
                }
              },
              ProtoReplay: {
                fields: {
                  time: {
                    rule: "required",
                    type: "sint32",
                    id: 1
                  },
                  type: {
                    rule: "required",
                    type: "ProtoReplayItemType",
                    id: 2
                  },
                  teamId: {
                    type: "sint32",
                    id: 3
                  },
                  item: {
                    type: "bytes",
                    id: 4
                  },
                  playerId: {
                    type: "sint64",
                    id: 5
                  }
                }
              },
              ProtoBlock: {
                fields: {
                  blockId: {
                    type: "sint32",
                    id: 1
                  },
                  x: {
                    type: "sint32",
                    id: 2
                  },
                  y: {
                    type: "sint32",
                    id: 3
                  },
                  breaking: {
                    type: "bool",
                    id: 4
                  },
                  config: {
                    type: "string",
                    id: 5
                  }
                }
              },
              ProtoResourceItem: {
                fields: {
                  type: {
                    rule: "required",
                    type: "sint32",
                    id: 1
                  },
                  amount: {
                    rule: "required",
                    type: "sint32",
                    id: 2
                  }
                }
              },
              ProtoCoreResources: {
                fields: {
                  item: {
                    rule: "repeated",
                    type: "ProtoResourceItem",
                    id: 1
                  }
                }
              },
              ProtoUnitChange: {
                fields: {
                  mechId: {
                    type: "sint32",
                    id: 1
                  }
                }
              },
              ProtoPlayerConnection: {
                fields: {
                  name: {
                    type: "string",
                    id: 1
                  }
                }
              },
              ProtoInfo: {
                fields: {
                  version: {
                    rule: "required",
                    type: "sint32",
                    id: 1
                  },
                  info: {
                    rule: "repeated",
                    type: "ProtoInfoRecord",
                    id: 4
                  }
                }
              },
              ProtoInfoRecord: {
                fields: {
                  name: {
                    rule: "required",
                    type: "ProtoInfoRecordName",
                    id: 1
                  },
                  value: {
                    type: "string",
                    id: 2
                  }
                }
              },
              ProtoResourceTransfer: {
                fields: {
                  itemId: {
                    type: "sint32",
                    id: 1
                  },
                  amount: {
                    type: "sint32",
                    id: 2
                  },
                  x: {
                    type: "sint32",
                    id: 3
                  },
                  y: {
                    type: "sint32",
                    id: 4
                  },
                  unitId: {
                    type: "sint32",
                    id: 5
                  }
                }
              },
              ProtoUnitPosition: {
                fields: {
                  unitId: {
                    type: "sint32",
                    id: 1
                  },
                  x: {
                    type: "sint32",
                    id: 2
                  },
                  y: {
                    type: "sint32",
                    id: 3
                  },
                  rotation: {
                    type: "float",
                    id: 4
                  }
                }
              },
              ProtoChat: {
                fields: {
                  message: {
                    type: "string",
                    id: 2
                  }
                }
              },
              ProtoUnit: {
                fields: {
                  unitId: {
                    type: "sint32",
                    id: 1
                  },
                  unitType: {
                    type: "sint32",
                    id: 2
                  },
                  x: {
                    type: "sint32",
                    id: 3
                  },
                  y: {
                    type: "sint32",
                    id: 4
                  },
                  rotation: {
                    type: "float",
                    id: 5
                  },
                  lastDamageByUnitType: {
                    type: "sint32",
                    id: 6
                  },
                  lastDamageByBlockType: {
                    type: "sint32",
                    id: 7
                  }
                }
              },
              ProtoPlayerState: {
                fields: {
                  unitId: {
                    type: "sint32",
                    id: 1
                  },
                  x: {
                    type: "sint32",
                    id: 2
                  },
                  y: {
                    type: "sint32",
                    id: 3
                  },
                  miningTileX: {
                    type: "sint32",
                    id: 4
                  },
                  miningTileY: {
                    type: "sint32",
                    id: 5
                  },
                  rotation: {
                    type: "float",
                    id: 6
                  },
                  buildingTileX: {
                    type: "sint32",
                    id: 7
                  },
                  buildingTileY: {
                    type: "sint32",
                    id: 8
                  },
                  buildingTileBlockId: {
                    type: "sint32",
                    id: 9
                  }
                }
              },
              ProtoTieBreaker: {
                fields: {
                  damage: {
                    type: "sint32",
                    id: 1
                  }
                }
              },
              ProtoGameOver: {
                fields: {
                  winnerTeam: {
                    type: "sint32",
                    id: 1
                  },
                  reason: {
                    type: "ProtoGameOverReason",
                    id: 2
                  }
                }
              },
              ProtoSchematicScheduled: {
                fields: {
                  name: {
                    type: "string",
                    id: 1
                  },
                  tier: {
                    type: "sint32",
                    id: 2
                  },
                  x: {
                    type: "sint32",
                    id: 3
                  },
                  y: {
                    type: "sint32",
                    id: 4
                  },
                  coreBuild: {
                    type: "sint32",
                    id: 5
                  }
                }
              }
            }
          }
        }
      }
    }
  }
});

module.exports = $root;
